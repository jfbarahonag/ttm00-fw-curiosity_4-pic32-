1. Project Brief:
1.1. Project summary: 
    This project contains the code of TITOMA's task, here you can find routines that use Timers and change notice interrupt that act over LEDs of the curiosity microchip board PIC32MX470F512H.
2. HW description:
2.1. Links to MCUs datasheet:
    - PIC32MX470F512H:
    http://ww1.microchip.com/downloads/en/devicedoc/60001185g.pdf
    - Curiosity Board user guide: http://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf
3. Serial commands:
3.1. Link to the commands source/header file:
    - main.c:
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Src/main.c
    - main.h:
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Inc/main.h
    - pic32_LEDS.c:
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Src/config_LEDS.c
    - pic32_LEDS.h:
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Inc/config_LEDS.h
    - pic32_uart.c
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Src/pic32_uart.c:
    - pic32_uart.h 
    https://bitbucket.org/jfbarahonag/ttm00-fw-curiosity_1-2-3-pic32/src/Develop/Inc/pic32_uart.h:
3.2. Serial command file for serial terminal: 
    - docklight https://docklight.de/downloads/
3.3. Serial configuration:
    - Baud rate = 115200
    - Parity = None
    - Parity Error Char. --- (ignore)
    - Data Bits = 8
    - Stop Bits = 1
    --Send/Receive Com --- The one assigned by your PC.
4. Prerequisites:
4.1. SDK version:
    - MPLAB V3.61
4.2. IDE:
    - MPLABX IDE V3.61
4.3. Compiler version:
    - XC 32 V1.42
4.4. Project configuration:
    - Categories : Microchip Embedded
    - Projects : Standalone Project
    - Family : All families
    - Device : PIC32MX470F512H
    - Hardware tools : Microchip starter kits
    - Compiler : XC32 (v1.42)
    - Encoding : ISO-8859-1
5. Versioning:
5.1. Current version of the fw/sw:
    - V2.2.20190906
5.2. Description of the last version changes:
    - Variables were organized inside structures.
    - Routines were organized in functions.
    - Code was commented following Doxygen tags standard.
    - UART parsing commands were changed as shown below:
        - SGMTxxxx  -> SET GREEN MACRO TIME ON (xxxx IS NUMERIC)
        - SGMPxxxx  -> SET GREEN MACRO PERIOD  (xxxx IS NUMERIC)
        - SGmTxx    -> SET GREEEN  micro TIME ON   (xx IS NUMERIC)
        - SGmPxx    -> SET GREEN micro PERIOD      (xx IS NUMERIC)
        - EDGLx     -> ENABLE/DISABLE GREEN LED (x IS BOOLEAN 0/1)
        - SBMTxxxx  -> SET BLUE MACRO TIME ON  (xxxx IS NUMERIC)
        - SBMPxxxx  -> SET BLUE MACRO PERIOD   (xxxx IS NUMERIC)
        - SBmTxx    -> SET BLUE micro TIME ON      (xx IS NUMERIC)
        - SBmPxx    -> SET BLUE micro PERIOD       (xx IS NUMERIC)
        - EDBLx     -> ENABLE/DISABLE BLUE LED (x IS BOOLEAN 0/1)
6. Authors:
    - Project staff: 
    Juan Felipe Barahona Gonzalez
    - Maintainer contact: 
    email: felipe.barahona@titoma.com