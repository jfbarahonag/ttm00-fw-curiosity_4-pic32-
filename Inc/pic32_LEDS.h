#include<plib.h>
#include <stdint.h>
#include <stdio.h>
#include<stdlib.h>
#include<xc.h>

enum{
    LED_OFF,
    LED_ON
};

enum _LED_selection_t{
    e_LED_1,
    e_LED_2,
    e_LED_3,
    e_LED_R,
    e_LED_G,
    e_LED_B
}LED_selection_t;

void LED_toggle(uint8_t LED);
void LED_on(uint8_t LED);
void LED_off(uint8_t LED);
void error_handler(void);
