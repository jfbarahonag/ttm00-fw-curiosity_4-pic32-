#include "config.h"
#include "pic32_LEDS.h" 
#include "pic32_uart.h"
#include <xc.h>
#include<stdlib.h>
#include <string.h>
#include <math.h>

#define FW_VER "1.2."
#define AUTHOR "FelipeBarahona"
//Timers configurations
#define SYS_FREQ        (96000000UL)
#define PBCLK_FREQUENCY        (96 * 1000 * 1000)
#define TICKS_PER_SEC   1000
#define CORE_TICK_RATE  (SYS_FREQ/2/TICKS_PER_SEC)
#define T1_TICK_RATE    (PBCLK_FREQUENCY/8/TICKS_PER_SEC)
 //Change notice configurations
#define CONFIG  (CND_ON | CND_IDLE_CON)
#define PINS    (CND6_ENABLE)
#define PULLUPS (CND_PULLUP_DISABLE_ALL)
#define INTERRUPT   (CHANGE_INT_PRI_2 | CHANGE_INT_ON)
//ADC configuration
#define PARAM1 ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
#define PARAM2 ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_ON
#define PARAM3 ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_12
#define PARAM5 ENABLE_AN14_ANA
#define PARAM4 SKIP_SCAN_ALL
//Boolean
#define TRUE    1
#define FALSE   0
//Parsing
#define GREEN_MACRO_ON      "GMAO"
#define GREEN_MACRO_PERIOD  "GMAP"
#define GREEN_MICRO_ON      "GMIO"
#define GREEN_MICRO_PERIOD  "GMIP"
#define GREEN_ENABLE        "GENB"
//
#define BLUE_MACRO_ON       "BMAO"
#define BLUE_MACRO_PERIOD   "BMAP"
#define BLUE_MICRO_ON       "BMIO"
#define BLUE_MICRO_PERIOD   "BMIP"
#define BLUE_ENABLE         "BENB"
//
#define GET_ADC             "GADC"
#define MYSELF              1
#define OTHER_BOARD         2
//
#define UPDATE_UART         1500
#define PERIOD_LED_2        1000
//
#define TIME_TO_TOGGLE_LED_1        500
#define TIME_TO_CHANGE_STATUS_LED_2 2000
//
#define MACRO_PERIOD_LED_3  2000
#define MACRO_TIME_ON_LED_3 1000
#define PWM_PERIOD_LED_3    20
#define PWM_TIME_ON_LED_3   2

#define NON_ERRORS          0
#define OFF                 0

#define ON                  1

#define FIRST_POSITION      0
#define ZERO                48
#define NINE                57
// Variables for LED 1 y LED 2
typedef volatile struct change_led_status_ {
    uint8_t timeout_reached;
    uint8_t led_must_be_change_status;
    uint16_t time_LED;
}change_led_status_t;

change_led_status_t led_1 = {
    .timeout_reached = OFF,
    .led_must_be_change_status = ON,
    .time_LED = TIME_TO_TOGGLE_LED_1
};

change_led_status_t led_2 = {
    .timeout_reached = OFF,
    .led_must_be_change_status = ON,
    .time_LED = TIME_TO_CHANGE_STATUS_LED_2
};
//CoreTimer variables structure 
typedef volatile struct times{
    uint16_t LED2_millis;
    uint16_t UART_millis;
    uint16_t SW1_LED1_millis;
    uint16_t SW1_LED2_millis;
}time_t;
//PWM configuration (Timer1) structure 
typedef volatile struct LED{
    uint16_t LED_pwm_on; //ms total time on
    uint16_t LED_pwm; //ms total time (period)
    uint16_t LED_macro;
    uint16_t LED_macro_on;
    uint16_t LED_pwm_on_aux;
    uint16_t LED_pwm_aux;
}LED_t;
 //Flags SW1 (CoreTimer) structure
typedef volatile struct flags{
    uint8_t button_pressed :1;
 }flag_t;
 //ADC structure
typedef struct ADC{
    uint32_t channel14;
    uint32_t offset;
    uint8_t BUFFER_UART1_TX[100];
}ADC_AN14_t;
//Variables used to establish times
typedef volatile struct _set_variables{
    uint16_t macro_on;
    uint16_t macro_period;
    uint16_t micro_on;
    uint16_t micro_period;
    uint8_t enable;
}set_variables_t;
//Variables used for parsing errors
typedef volatile union {
    struct {
        uint8_t macro_error :1;
        uint8_t micro_error :1;
        uint8_t period_error:1;
        uint8_t time_on_error:1;
    };
    struct {
        uint8_t all_errors  :4;
    };
}errors_t;

typedef volatile struct _parsing{
    uint8_t command [U1RxBufSize];
    uint16_t value;
    uint16_t board;
    uint8_t message [U1RxBufSize];
}parsing_t;

void main_setup(void);
void ADC_setup(void); 
void init_setup(void);
void interrupts_setup(void);
void init_structure_variables(void);
void init_structure_green_LED_variables(void);
void init_structure_blue_LED_variables(void);

uint32_t read_adc(void);
void communication_UART_coretimer(void);
void toggle_LED_SW1 (uint8_t led_selection);
void toggle_LED_1_SW1_pressed_500_ms_coretimer(void);
void toggle_LED_2_SW1_pressed_2000_ms_coretimer(void);
void blink_LED_3_with_pwm_timerone(void);
void is_message_received_by_UARTx(void);
void by_UART_1(void);
void by_UART_2(void);
void process_data_UART1(void);
void process_data_UART2(void);
uint8_t is_a_number(void);
void select_board_UART1(void);
void select_board_UART2(void);
void reset_Rx_Tx_buffers(void);
void select_external_command(void);
void select_internal_command(void);
uint8_t green_LED_comparison(void);
uint8_t blue_LED_comparison(void);
void save_value_received(void);
void errors_setting_time(errors_t *t_errors, set_variables_t *variable_LED);
void set_LED_values(errors_t * t_errors, set_variables_t *set_LED_variable, LED_t *set_LED_value);
void UARTx_error_message(uint8_t *ARG_UART);
void save_led_variables(set_variables_t * var_source, LED_t * var_dest );
void show_variables_UART2(void);
uint8_t get_adc_comparison(void);
void blink_LED_with_pwm(LED_t * LED_characteristic, uint8_t LED_selection);
uint8_t time_on(uint16_t macro_on,uint16_t macro,uint16_t micro_on,uint16_t micro);
