/* ************************************************************************** */
/** Descriptive File Name
 
 @Company
 Company Name
 
 @File Name
 filename.h
 
 @Summary
 Brief description of the file.
 
 @Description
 Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _RING_BUFFER_H    /* Guard against multiple inclusion */
#define _RING_BUFFER_H

#include <stdint.h>
#include <stddef.h>

#define TRUE    1
#define FALSE   0
// *****************************************************************************
typedef struct _ring_buffer_{
    uint8_t     head;
    uint8_t     tail;
    uint8_t     max;
    uint8_t     *buffer;
    uint8_t     full;
}ring_buffer_t;

typedef enum put_error_ {
    PUT_ERROR_NONE = 0,
    PUT_ERROR_WRONG_BUFFER,
    PUT_ERROR_FULL_BUFFER,
}put_error_t;

typedef enum get_error_ {
    GET_ERROR_NONE = 0,
    GET_ERROR_WRONG_BUFFER,
    GET_ERROR_EMPTY_BUFFER,
    GET_ERROR_NO_DATA
}get_error_t;

uint8_t ring_buffer_init(ring_buffer_t *ring_buffer, uint8_t *buffer, uint8_t size);
uint8_t ring_buffer_is_full(ring_buffer_t *ring_buffer);
put_error_t ring_buffer_put(ring_buffer_t *ring_buffer, uint8_t data);
get_error_t ring_buffer_get(ring_buffer_t *ring_buffer, uint8_t *data);
/* *****************************************************************************
 End of File
 */
#endif
