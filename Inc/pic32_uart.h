#ifndef _PIC32_UART_H
#define	_PIC32_UART_H

#include <stdint.h>

#ifndef SYS_FREQ
#define SYS_FREQ			(96000000UL)
#endif 
#ifndef PBCLK_FREQUENCY
#define PBCLK_FREQUENCY		(96 * 1000 * 1000)
#endif

#define PRINT_MESSAGE(ARG1, ARG2)  sprintf((char *)U1TxBuf, ARG1, ARG2);\
                    UART1_write_string((const char *)U1TxBuf)

#define PRINT_MESSAGE_SIMPLE(ARG1)  sprintf((char *)U1TxBuf, ARG1);\
                    UART1_write_string((const char *)U1TxBuf)

#define PRINT_MESSAGE_U2(ARG1, ARG2)  sprintf((char *)U2TxBuf, ARG1, ARG2);\
                    UART2_write_string((const char *)U2TxBuf)
                    

#define PRINT_MESSAGE_SIMPLE_U2(ARG1)  sprintf((char *)U2TxBuf, ARG1);\
                    UART2_write_string((const char *)U2TxBuf)

#define U1RxBufSize 64
#define U1TxBufSize 64

#define U2RxBufSize 64
#define U2TxBufSize 64

extern unsigned char U1TxBuf[U1TxBufSize];
extern unsigned char U1RxBuf[U1RxBufSize];
extern unsigned char U1Unread;

extern unsigned char U2TxBuf[U2TxBufSize];
extern unsigned char U2RxBuf[U2RxBufSize];
extern unsigned char U2Unread;

void UART1_init(unsigned long int UART_Baud);
void UART1_write_string(const char *string);
void UART1_write_char(const char ch);

void UART1_reset_rx_buffer(void);
unsigned char UART1_is_unread(void);
void UART1_clear_unread(void);
void UART1_get_string(void);
////////////////////////////////////////////////////////////////////////////////
void UART2_init(unsigned long int UART_Baud);
void UART2_write_string(const char *string);
void UART2_write_char(const char ch);

void UART2_reset_rx_buffer(void);
unsigned char UART2_is_unread(void);
void UART2_clear_unread(void);
void UART2_get_string(void);

uint8_t uart1_get_length (void);
uint8_t uart2_get_length (void);


#endif	/* UART_H */
