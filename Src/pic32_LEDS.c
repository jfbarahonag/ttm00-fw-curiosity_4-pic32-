#include "pic32_LEDS.h"

void LED_on(uint8_t LED){
    if (LED==e_LED_1 || LED==e_LED_2 || LED==e_LED_3 || LED==e_LED_R || LED==e_LED_G || LED==e_LED_B){
    switch (LED){
        case e_LED_1:
            mPORTESetBits(BIT_4);
            break;
        case e_LED_2:
            mPORTESetBits(BIT_6);
            break;
        case e_LED_3:
            mPORTESetBits(BIT_7);
            break;
        case e_LED_R:
            mPORTBClearBits(BIT_10);
            break;
        case e_LED_G:
            mPORTBClearBits(BIT_3);
            break;
        case e_LED_B:
            mPORTBClearBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else 
        error_handler();
}

void LED_off(uint8_t LED){
    if (LED==e_LED_1 || LED==e_LED_2 || LED==e_LED_3|| LED==e_LED_R || LED==e_LED_G || LED==e_LED_B){
    switch (LED){
        case e_LED_1:
            mPORTEClearBits(BIT_4);
            break;
        case e_LED_2:
            mPORTEClearBits(BIT_6);
            break;
        case e_LED_3:
            mPORTEClearBits(BIT_7);
            break;
        case e_LED_R:
            mPORTBSetBits(BIT_10);
            break;
        case e_LED_G:
            mPORTBSetBits(BIT_3);
            break;
        case e_LED_B:
            mPORTBSetBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else 
        error_handler();
}

void LED_toggle(uint8_t LED){
    if (LED==e_LED_1 || LED==e_LED_2 || LED==e_LED_3|| LED==e_LED_R || LED==e_LED_G || LED==e_LED_B){
        switch (LED){
        case e_LED_1:
            mPORTEToggleBits(BIT_4);
            break;
        case e_LED_2:
            mPORTEToggleBits(BIT_6);
            break;
        case e_LED_3:
            mPORTEToggleBits(BIT_7);
            break;
        case e_LED_R:
            mPORTBToggleBits(BIT_10);
            break;
        case e_LED_G:
            mPORTBToggleBits(BIT_3);
            break;
        case e_LED_B:
            mPORTBToggleBits(BIT_2);
            break;
        default:
            break;
        }
    }
    else    
        error_handler();  
}

void error_handler(void){
    printf("Error");
    while(1){}
}