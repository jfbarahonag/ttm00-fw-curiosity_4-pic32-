/************************************************
 * @file      main.c
 * @author    JFBG
 * @version   V2.1.1
 * @date      15/08/2019
 * @brief     Toggle LEDs, use of SW1, Coretimer, Timer1, UART communication
 
 /mainpage Description
 This code toggles LEDs by switch SW1 making use of CoreTimer, TimerOne and 
 * Interrupt on change, also TimerOne is used for Blink and PWM. RGB LED is used
 * to blink by UART RX commands.
 ***********************************************/
#include"main.h"

time_t          time;
LED_t           LED_3_pwm;
LED_t           LED_green_pwm;
LED_t           LED_blue_pwm;
flag_t          flag_LED_change;
ADC_AN14_t      ADC_variables;
set_variables_t green_LED_variables;
set_variables_t blue_LED_variables;
errors_t        green_LED_errors;
errors_t        blue_LED_errors;
parsing_t       parsing;      
/*
 *  @brief   Main code 
 *  @param      <void>
 *  @retval     <void>
 */
int main(void){
    main_setup();
    while(1){
        read_adc();
        communication_UART_coretimer();
        toggle_LED_SW1(e_LED_R);
        toggle_LED_1_SW1_pressed_500_ms_coretimer();
        toggle_LED_2_SW1_pressed_2000_ms_coretimer();
        blink_LED_3_with_pwm_timerone();
        is_message_received_by_UARTx();
        blink_LED_with_pwm(&LED_blue_pwm, e_LED_B);
        blink_LED_with_pwm(&LED_green_pwm, e_LED_G);
    }
    return FALSE;
}
/*
 *  @brief Starts initial parameters  
 *  @param      <void>
 *  @retval     <void>
 */
void main_setup(void){
    ADC_setup();
    interrupts_setup();
    init_setup();   
    init_structure_variables();
}  
/*
 *  @brief ADC configurations  
 *  @param      <void>
 *  @retval     <void>
 */
void ADC_setup(void){
    mPORTBSetPinsAnalogIn(BIT_14);
    CloseADC10();
    SetChanADC10( ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN14);
    OpenADC10( PARAM1, PARAM2, PARAM3, PARAM4, PARAM5 );
    EnableADC10();
}  
/*
 *  @brief Configurations of interrupts
 *  @param      <void>
 *  @retval     <void>
 */
void interrupts_setup(void){
    //CoreTimer config
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_3 | CT_INT_SUB_PRIOR_0);
    INTConfigureSystem(INT_SYSTEM_CONFIG_MULT_VECTOR);
    INTEnableInterrupts();
    //TimerOne config
    OpenTimer1((T1_ON | T1_PS_1_8), T1_TICK_RATE);
    ConfigIntTimer1((T1_INT_ON | T1_INT_PRIOR_3 | T1_INT_SUB_PRIOR_3));
    //Interrupt on change
    mCNDOpen(CONFIG, PINS, PULLUPS);
    ConfigIntCND(INTERRUPT);
    INTEnableSystemMultiVectoredInt();
}   
/*
 *  @brief Initial configurations
 *  @param      <void>
 *  @retval     <void>
 */
void init_setup(void){
    printf("Booting ""FW_VER""-""AUTHOR");
    //UART
    UART1_init(115200);
    //    UART1_write_string("Initializing UART1 \n");
    UART2_init(115200);
    UART2_write_string("Initializing UART2 \n");
    // PORTB
    mPORTBSetPinsDigitalOut(BIT_2|BIT_3|BIT_10);
    // PORTE
    mPORTESetPinsDigitalOut(BIT_4|BIT_6|BIT_7);
    // PORTD(SW1)
    mPORTDSetPinsDigitalIn(BIT_6); 
    // Initial state
    LED_off(e_LED_1);
    LED_off(e_LED_2);
    LED_off(e_LED_3); 
    LED_off(e_LED_R);
    LED_off(e_LED_G);
    LED_off(e_LED_B); 
}    
/*
 *  @brief Initialize general variable structures
 *  @param      <void>
 *  @retval     <void>
 */
void init_structure_variables(void){
    time.LED2_millis = PERIOD_LED_2;
    time.UART_millis = UPDATE_UART;
    time.SW1_LED1_millis = TIME_TO_TOGGLE_LED_1;
    time.SW1_LED2_millis = TIME_TO_CHANGE_STATUS_LED_2;
    
    LED_3_pwm.LED_pwm_on = PWM_TIME_ON_LED_3;
    LED_3_pwm.LED_pwm = PWM_PERIOD_LED_3;
    LED_3_pwm.LED_macro = MACRO_PERIOD_LED_3;
    LED_3_pwm.LED_macro_on = MACRO_TIME_ON_LED_3;    
    LED_3_pwm.LED_pwm_aux = LED_3_pwm.LED_pwm;
    
    flag_LED_change.button_pressed = 0;
    
    init_structure_green_LED_variables();
    init_structure_blue_LED_variables();
    
    green_LED_errors.all_errors = NON_ERRORS;
    blue_LED_errors.all_errors = NON_ERRORS;
}
/*
 *  @brief    Initialize green LED variables
 *  @param      <void>
 *  @retval     <void>
 */
void init_structure_green_LED_variables(void){
    LED_green_pwm.LED_macro_on = OFF;
    LED_green_pwm.LED_macro = OFF;
    LED_green_pwm.LED_pwm_on = OFF;
    LED_green_pwm.LED_pwm = OFF;
}
/*
 *  @brief    Initialize blue LED variables
 *  @param      <void>
 *  @retval     <void>
 */
void init_structure_blue_LED_variables(void){
    LED_blue_pwm.LED_macro_on = OFF;
    LED_blue_pwm.LED_macro = OFF;
    LED_blue_pwm.LED_pwm_on = OFF;
    LED_blue_pwm.LED_pwm = OFF;
}
/*                    Interruption routines                                   */
/*
 *  @brief    Core timer routine
 *  @param      <void>
 *  @retval     <void>
 */
void __ISR(_CORE_TIMER_VECTOR, IPL3AUTO) _CoreTimerHandler(void)
{
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);
    //Each certain time
    if(time.LED2_millis) 
        time.LED2_millis--;   
    if(time.UART_millis)  
        time.UART_millis--;  
    //  If SW1 is pressed   
    if ((flag_LED_change.button_pressed != OFF)) {
        //  if button still pressed and time not reached the time needed to change the status
        if (led_1.timeout_reached == 0) {
            //  Decides if timeout was reached 
            if (led_1.time_LED == TIME_TO_TOGGLE_LED_1) {
                led_1.time_LED--;
            } else if (led_1.time_LED < TIME_TO_TOGGLE_LED_1 && led_1.time_LED > OFF) {
                led_1.time_LED--;
            }   else {
                led_1.timeout_reached = ON;
                led_1.led_must_be_change_status = ON;
            }    
        }
        //  if button still pressed and time not reached the time needed to change the status
        if (led_2.timeout_reached == 0) {
            //  Decides if timeout was reached 
            if (led_2.time_LED == TIME_TO_CHANGE_STATUS_LED_2) {
                led_2.time_LED--;
            } else if (led_2.time_LED < TIME_TO_CHANGE_STATUS_LED_2 && led_2.time_LED > OFF) {
                led_2.time_LED--;
            }   else {
                led_2.timeout_reached = ON;
                led_2.led_must_be_change_status = !led_2.led_must_be_change_status;
            }
        }  
    } else {
        led_1.timeout_reached = 0;
        led_1.time_LED = TIME_TO_TOGGLE_LED_1;
        led_2.timeout_reached = 0;
        led_2.time_LED = TIME_TO_CHANGE_STATUS_LED_2;
    }
}
/*
 *  @brief    Timer one routine
 *  @param      <void>
 *  @retval     <void>
 */
void __ISR(_TIMER_1_VECTOR, IPL3AUTO) Timer1Handler(void)
{
    mT1ClearIntFlag();
    WritePeriod1(T1_TICK_RATE);
    //Each certain time 
    if(LED_3_pwm.LED_pwm != OFF)  
        LED_3_pwm.LED_pwm--;
    if(LED_3_pwm.LED_macro != OFF) 
        LED_3_pwm.LED_macro--;
    //Blinking time period
    if(LED_3_pwm.LED_macro == OFF)
        LED_3_pwm.LED_macro = MACRO_PERIOD_LED_3;   
    //PWM configuration
    if(LED_3_pwm.LED_pwm == OFF)
        LED_3_pwm.LED_pwm = PWM_PERIOD_LED_3;
    //Countdown is available when it has not establishment errors and flag to start is activated
    if(green_LED_variables.enable && !green_LED_errors.macro_error && !green_LED_errors.micro_error){
        if(LED_green_pwm.LED_macro)
            LED_green_pwm.LED_macro--;
        if(LED_green_pwm.LED_pwm)
            LED_green_pwm.LED_pwm--;
        if(!LED_green_pwm.LED_macro)
            LED_green_pwm.LED_macro = green_LED_variables.macro_period;      
        if(!LED_green_pwm.LED_pwm)
            LED_green_pwm.LED_pwm   = green_LED_variables.micro_period;     
    } 
    //Countdown is available when it has not establishment errors and flag to start is activated
    if(blue_LED_variables.enable && !blue_LED_errors.macro_error && !blue_LED_errors.micro_error){
        if(LED_blue_pwm.LED_macro)
            LED_blue_pwm.LED_macro--;
        if(LED_blue_pwm.LED_pwm)
            LED_blue_pwm.LED_pwm--;
        if(!LED_blue_pwm.LED_macro)
            LED_blue_pwm.LED_macro  = blue_LED_variables.macro_period;      
        if(!LED_blue_pwm.LED_pwm)
            LED_blue_pwm.LED_pwm    = blue_LED_variables.micro_period;               
    }    

}


/*
 *  @brief    Change notice routine
 *  @param      <void>
 *  @retval     <void>
 */
void __ISR(_CHANGE_NOTICE_VECTOR, IPL2AUTO) ChangeNotice_Handler(void)
{
    mPORTDReadBits(BIT_6);
    flag_LED_change.button_pressed = !mPORTDReadBits(BIT_6);
    mCNDClearIntFlag();
}
/*                    Interruption routines                                   */
/*
 *  @brief    Obtains adc value and returns it
 *  @param      <void>
 *  @retval     <ADC_variables.channel14> Value among 0 and 1023
 */
uint32_t read_adc(void){   
    ADC_variables.offset = 8 * ((~ReadActiveBufferADC10() & 0x01));
    ADC_variables.channel14 = ReadADC10(ADC_variables.offset);
    return ADC_variables.channel14;
}
/*
 *  @brief    when 1500 mseg were reached it value was printed
 *  @param      <void>
 *  @retval     <ADC_variables.channel14> Value among 0 and 1023
 */
void communication_UART_coretimer(void){
    if(!time.UART_millis){
        PRINT_MESSAGE_U2("Value = %d \r\n",(int)ADC_variables.channel14);
        time.UART_millis = UPDATE_UART;
    }
}
/*
 *  @brief    Each time SW1 is pressed, it changes it's status
 *  @param      <void>
 *  @retval     <void>
 */
void toggle_LED_SW1 (uint8_t led_selection) {
    if (flag_LED_change.button_pressed != FALSE) {
        LED_toggle(led_selection);
    } 
}    
/*
 *  @brief    If SW1 is pressed at least 0.5 seconds LED1 toggle
 *  @param      <void>
 *  @retval     <void>
 */
void toggle_LED_1_SW1_pressed_500_ms_coretimer(void){
    //LED1 toggle if SW1 is pressed at least 500mSecs
    if(led_1.led_must_be_change_status != 0) {
        led_1.led_must_be_change_status = 0;
        LED_toggle(e_LED_1); 
    }  
}
//
/*
 *  @brief      If SW1 is pressed at least 2 seconds LED2 can start or stop blink 
 *  @param      <void>
 *  @retval     <void>
 */
void toggle_LED_2_SW1_pressed_2000_ms_coretimer(void){
    //LED2 toggle if SW1 is pressed at least 2000mSecs
    if(led_2.led_must_be_change_status != 0) {
        if(!time.LED2_millis){
            time.LED2_millis = PERIOD_LED_2;
            LED_toggle(e_LED_2);  
        }
    }
}  
/*
 *  @brief      LED 3 Blink with PWM
 *  @param      <void>
 *  @retval     <void>
 */
void blink_LED_3_with_pwm_timerone(void){
    //LED3 is blinking with PWM functionality
    LED_3_pwm.LED_pwm_on_aux = (LED_3_pwm.LED_pwm_aux*ADC_variables.channel14)/1023;
    if(time_on(LED_3_pwm.LED_macro_on,LED_3_pwm.LED_macro,LED_3_pwm.LED_pwm_on_aux,LED_3_pwm.LED_pwm))     
        LED_on(e_LED_3);
    else
        LED_off(e_LED_3);
} 
/*
 *  @brief      Check if UART RX received data
 *  @param      <void>
 *  @retval     <void>
 */
void is_message_received_by_UARTx(void){
    by_UART_1();
    by_UART_2();
}
/*
 *  @brief      If data is received by UART 1 (OTHER BOARD)
 *  @param      <void>
 *  @retval     <void>
 */
void by_UART_1(void){
    if (UART1_is_unread()){
        UART1_clear_unread();
        memcpy((void*)parsing.message,(const void *)U1RxBuf, U1RxBufSize);
        UART2_write_string((const char *)"\nMessage received by Board 2:\n\n");      
        process_data_UART1();
    }
}
/*
 *  @brief      If data is received by UART 2 (MYSELF)
 *  @param      <void>
 *  @retval     <void>
 */
void by_UART_2(void){
    if (UART2_is_unread()){
        UART2_clear_unread(); 
        UART1_reset_rx_buffer();
        memcpy((void*)parsing.message,(const void *)U2RxBuf, U2RxBufSize);
        UART2_reset_rx_buffer();
        process_data_UART2();
    }
}
/*
 *  @brief      Process required to data received by UART 1
 *  @param      <void>
 *  @retval     <void>
 */
void process_data_UART1(void){
    if (is_a_number()){
        PRINT_MESSAGE("Message: %s\n\r", parsing.message);
        sscanf((const char *)parsing.message,"%hu %s %hu",&parsing.board,parsing.command,&parsing.value);
        sprintf((char *)U2TxBuf,(const char *)"[Board 2] Command: %s Value: %d\n\r",parsing.command,parsing.value);
        UART2_write_string((const char *)U2TxBuf);
        select_board_UART1();
    }
    else{
        PRINT_MESSAGE_U2 ("[Board 2] %s",parsing.message);
    }      
    reset_Rx_Tx_buffers();
}
/*
 *  @brief      Process required to data received by UART 2
 *  @param      <void>
 *  @retval     <void>
 */
void process_data_UART2(void){
    if (is_a_number()){
        sscanf((const char *)parsing.message,"%hu %s %hu",&parsing.board,parsing.command,&parsing.value);
        select_board_UART2();
    }
    else{
        PRINT_MESSAGE("[Pipe] %s",parsing.message);
    }
    reset_Rx_Tx_buffers();
}
/*
 *  @brief      The parsing process requires that the first character be a number to select the source of the message
 *              
 *  @param      <void>
 *  @retval     <void>
 */
uint8_t is_a_number(void){
    uint8_t is_character_a_number = (parsing.message[FIRST_POSITION] >= ZERO && parsing.message[0] <= NINE); 
    return is_character_a_number;
}
//@Brief    Detect if is a valid board (received by UART 1)
void select_board_UART1(void){
    if (parsing.board == OTHER_BOARD){
        select_external_command();
    }
    else{
        PRINT_MESSAGE_SIMPLE("[Pipe] Board not recognized \n\r");
    }
}
//@Brief    Detect if is a valid board (received by UART 2)
void select_board_UART2(void){
    if(parsing.board == MYSELF){
        select_internal_command();
    }
    else if(parsing.board == OTHER_BOARD){
        PRINT_MESSAGE("%s", parsing.message);
    }
    else{
        PRINT_MESSAGE_SIMPLE_U2("[Pipe] Board not recognized \n");
    }
}
//@Brief    Reset Tx and RX buffers
void reset_Rx_Tx_buffers(void){
    memset(U1TxBuf,0,U1TxBufSize);
    memset(U2TxBuf,0,U2TxBufSize);
    memset((void*)parsing.message,0,U1RxBufSize);
    UART1_reset_rx_buffer();
    UART2_reset_rx_buffer();
}
//@Brief     Analyze if the command received by board 2 is correct and save (UART1)
void select_external_command(void){
    if (green_LED_comparison()||blue_LED_comparison()){
        save_value_received();
        show_variables_UART2();
    }
    else if(get_adc_comparison()){
        memset (U1TxBuf, 0, U1TxBufSize);
        memset (U2TxBuf, 0, U2TxBufSize);
        PRINT_MESSAGE   ("[Pipe] ADC value : %d\n\r", ADC_variables.channel14);
        PRINT_MESSAGE_U2("[Pipe] ADC value = %d\n\r", ADC_variables.channel14);
    }
    else{
        UART1_write_string("[Pipe] Error: Wrong command \n\r");
    }
}
//@Brief    Analyze the command received by user (UART2)
void select_internal_command(void){
    if (green_LED_comparison()||blue_LED_comparison()){
        save_value_received();
        show_variables_UART2();
    }
    else if(get_adc_comparison()){            
        PRINT_MESSAGE_U2("[Pipe] ADC value = %d \n", ADC_variables.channel14);
        memset(U2TxBuf,0,U2TxBufSize);
    }
    else{
        PRINT_MESSAGE_SIMPLE_U2("Wrong command \n\r");
    }
}
//@Brief    Compare command with pre-established commands (GREEN LED)
uint8_t green_LED_comparison(void){
    if (!memcmp((void *)parsing.command, GREEN_MACRO_ON,4)) {
        green_LED_variables.macro_on        = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, GREEN_MACRO_PERIOD,4)) {
        green_LED_variables.macro_period    = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, GREEN_MICRO_ON,4)) {
        green_LED_variables.micro_on        = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, GREEN_MICRO_PERIOD,4)) {
        green_LED_variables.micro_period    = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, GREEN_ENABLE,4)) {
        green_LED_variables.enable          = parsing.value;
        return TRUE;
    } else {
        return FALSE;
    }
}
//@Brief    Compare command with pre-established commands (BLUE LED)
uint8_t blue_LED_comparison(void){
    if (!memcmp((void *)parsing.command, BLUE_MACRO_ON,4)) {
        blue_LED_variables.macro_on        = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, BLUE_MACRO_PERIOD,4)) {
        blue_LED_variables.macro_period    = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, BLUE_MICRO_ON,4)) {
        blue_LED_variables.micro_on        = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, BLUE_MICRO_PERIOD,4)) {
        blue_LED_variables.micro_period    = parsing.value;
        return TRUE;
    } else if (!memcmp((void *)parsing.command, BLUE_ENABLE,4)) {
        blue_LED_variables.enable          = parsing.value;
        return TRUE;
    } else {
        return FALSE;   
    }
}
//@Brief    Save value in temporal variables and show them  
void save_value_received(void){  
    errors_setting_time(&green_LED_errors, &green_LED_variables);
    errors_setting_time(&blue_LED_errors, &blue_LED_variables);
    set_LED_values(&green_LED_errors, &green_LED_variables, &LED_green_pwm);
    set_LED_values(&blue_LED_errors, &blue_LED_variables, &LED_blue_pwm);
}
//@brief    Detect time errors
void errors_setting_time(errors_t *t_errors, set_variables_t *variable_LED){
    if(variable_LED->macro_on > variable_LED->macro_period){
        t_errors->macro_error      = 1;
    } else if(variable_LED->micro_on > variable_LED->micro_period){
        t_errors->micro_error      = 1;
    } else if(variable_LED->micro_period > variable_LED->macro_period){
        t_errors->period_error     = 1;
    } else{       
        t_errors->all_errors       = 0;
    }
}
//@Brief    Usage: Recognize errors
typedef enum{
            NO_ERRORS               = 0,
            LED_MACRO_TIME_ERROR    = 1,
            LED_micro_TIME_ERROR    = 2,
            LED_PERIOD_ERROR        = 4,
            TIME_ON_ERROR           = 8
}errors_setting_times;
//@brief    Save or not temp variables if exist at least one error
void set_LED_values(errors_t * t_errors, set_variables_t *set_LED_variable, LED_t *set_LED_value) {
    switch(t_errors->all_errors) {
        case NO_ERRORS:
            save_led_variables(set_LED_variable, set_LED_value);
            break;
        case LED_MACRO_TIME_ERROR:
            UARTx_error_message((uint8_t *)"Error setting LED: Macro period must be greater than Macro time on. Macro variables will be CLEARED\n\r");
            set_LED_variable->macro_on = 0;
            set_LED_variable->macro_period = 0;
            save_led_variables(set_LED_variable, set_LED_value);
            t_errors->macro_error   = 0;
            break;
        case LED_micro_TIME_ERROR:
            UARTx_error_message((uint8_t *)"Error setting LED: Micro period must be greater than micro time on. Micro variables will be CLEARED\n\r");
            set_LED_variable->micro_on = 0;
            set_LED_variable->micro_period = 0;
            save_led_variables(set_LED_variable, set_LED_value);
            t_errors->micro_error   = 0;
            break;
        case LED_PERIOD_ERROR:
            UARTx_error_message((uint8_t *)"Error setting LED: Macro period must be greater than micro period. All variables will be CLEARED\n\r\n\r");
            set_LED_variable->micro_on = 0;
            set_LED_variable->macro_on = 0;
            set_LED_variable->micro_period = 0;
            set_LED_variable->macro_period = 0;
            save_led_variables(set_LED_variable, set_LED_value);
            t_errors->period_error  = 0;
            break;
        case TIME_ON_ERROR:
            UARTx_error_message((uint8_t *)"Error setting LED: Macro time on must be greater than micro time on. All variables will be CLEARED\n\r\n\r");
            set_LED_variable->micro_on = 0;
            set_LED_variable->macro_on = 0;
            set_LED_variable->micro_period = 0;
            set_LED_variable->macro_period = 0;
            save_led_variables(set_LED_variable, set_LED_value);
            t_errors->time_on_error  = 0;
        default:
            break;
    }
}
//@Brief    Save in xLED PWM variables
void save_led_variables(set_variables_t * var_source, LED_t * var_dest ) {
    var_dest -> LED_macro_on    = var_source -> macro_on;
    var_dest -> LED_macro       = var_source -> macro_period;
    var_dest -> LED_pwm_on      = var_source -> micro_on;
    var_dest -> LED_pwm         = var_source -> micro_period;
}
//@Brief    Select UART channel to send message 
void UARTx_error_message(uint8_t *ARG_UART){
    if (parsing.board == MYSELF) {
        UART2_write_string((const char *)ARG_UART);
    }
    else{
        UART1_write_string((const char *)ARG_UART);
    }
}
//@Brief    Print to user the time values modified
void show_variables_UART2(void){
    PRINT_MESSAGE_U2("GREEN MACRO ON TIME  %d \n"  , green_LED_variables.macro_on);
    PRINT_MESSAGE_U2("GREEN MACRO PERIOD   %d \n"  , green_LED_variables.macro_period);
    PRINT_MESSAGE_U2("GREEN MICRO ON TIME  %d \n"  , green_LED_variables.micro_on);
    PRINT_MESSAGE_U2("GREEN MICRO PERIOD   %d \n\r"  , green_LED_variables.micro_period);
    PRINT_MESSAGE_U2("BLUE MACRO ON TIME   %d \n"  , blue_LED_variables.macro_on);
    PRINT_MESSAGE_U2("BLUE MACRO PERIOD    %d \n"  , blue_LED_variables.macro_period);
    PRINT_MESSAGE_U2("BLUE MICRO ON TIME   %d \n"  , blue_LED_variables.micro_on);
    PRINT_MESSAGE_U2("BLUE MICRO PERIOD    %d \n\r"  , blue_LED_variables.micro_period);   
}
//@Brief    Compare command with pre-established commands (GET ADC VALUE)
uint8_t get_adc_comparison(void){
    return !memcmp((void*)parsing.command,GET_ADC,4);  
}
//@brief    XLED Blink with PWM
void blink_LED_with_pwm(LED_t * LED_characteristic, uint8_t LED_selection){
    LED_off(LED_selection);
    if(time_on(LED_characteristic->LED_macro_on, LED_characteristic->LED_macro, LED_characteristic->LED_pwm_on, LED_characteristic->LED_pwm)) {
        LED_on ( LED_selection );
    } else {
        LED_off ( LED_selection );
    }
}
//@brief    Conditions to turn ON RGB LED
uint8_t time_on(uint16_t macro_on,uint16_t macro,uint16_t micro_on,uint16_t micro){
    uint16_t macro_Period = macro;
    uint16_t macro_ON_TIME = macro_on;
    uint16_t micro_Period = micro;
    uint16_t micro_ON_TIME = micro_on;
    uint8_t LED_turn_ON = (macro_Period<macro_ON_TIME)&&(micro_Period<micro_ON_TIME);
    return LED_turn_ON;
}