/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include "ring_buffer.h"

ring_buffer_t   ring_buffer;

uint8_t ring_buffer_init(ring_buffer_t *ring_buffer, uint8_t *buffer, uint8_t max){
    
    if (max == 0) {
        return 1;
    } else if (buffer == NULL) {
        return 1;
    } else if (ring_buffer == NULL) {
        return 1;
    }   
    ring_buffer->buffer = buffer;   //apunta a la direccion del espacio de memoria
    ring_buffer -> head = 0;
    ring_buffer -> tail = 0;
    ring_buffer -> max = max;
    ring_buffer -> full = 0;
    return 0;
}

uint8_t ring_buffer_is_full(ring_buffer_t *ring_buffer) {
    if (ring_buffer->head == ring_buffer->tail) {
        ring_buffer->full = 1;
    }
    return ring_buffer->full;
}

put_error_t ring_buffer_put(ring_buffer_t *ring_buffer, uint8_t data){
    if (ring_buffer -> buffer == NULL) {
        return 1;
    }
    ring_buffer->buffer[ring_buffer->head] = data;
    ring_buffer->head++;
    
    if (ring_buffer->full != 0) {
        ring_buffer->tail++;
        
        if (ring_buffer->tail >= ring_buffer->max){
            ring_buffer->tail = 0;
        }
    }
    
    if (ring_buffer->head >= ring_buffer->max) {
        ring_buffer->head = 0;
    }
    
    
    if (ring_buffer->head == ring_buffer->tail) {
        ring_buffer->full = 1;
        return 2;
    }
                    
    return 0;
}

get_error_t ring_buffer_get(ring_buffer_t *ring_buffer, uint8_t *data){
    if ( ring_buffer -> buffer == NULL ) {
        return GET_ERROR_WRONG_BUFFER;
    } else if ( (ring_buffer -> full == 0) && (ring_buffer -> head == ring_buffer -> tail) ) {
        return GET_ERROR_EMPTY_BUFFER;
    } else if ( data == NULL ) {
        return GET_ERROR_NO_DATA;
    }
    
    *data = ring_buffer -> buffer[ring_buffer -> tail];
    ring_buffer->tail++;
    ring_buffer->full = 0;
    if ( ring_buffer -> tail == ring_buffer -> max ) {
        ring_buffer->tail = 0;
    }
    return GET_ERROR_NONE;
}